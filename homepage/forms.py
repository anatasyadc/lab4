from django import forms
from .models import Schedule
from django.forms import ModelForm

class ScheduleForm(ModelForm):
	class Meta:
		model= Schedule
		fields = ['activity','dates', 'time', 'location', 'category']
		labels = {
			'activity' : 'Activity','dates' : 'Date', 'time' : 'Time', 'location' : 'Location', 'category' : 'Category'
		}
		widgets = {
			'activity' : forms.TextInput(attrs={'class': 'form-control',
										'type' : 'text',
										'placeholder' : 'Apa yang ingin kamu lakukan?'}),
			'dates' : forms.DateInput(attrs={'class': 'form-control',
										'type' : 'date'}),
			'time' : forms.TimeInput(attrs={'class': 'form-control',
										'type' : 'time'}),
			
			'location' : forms.TextInput(attrs={'class' : 'form-control',
										'type' : 'text',
										'placeholder' : 'Dimanakah tempatnya?'}),
			'category' : forms.Select(attrs={'class': 'form-control'})
		}


