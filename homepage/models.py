from django.db import models
import datetime

Category=[('Tugas','Tugas'),('Kuis','Kuis'),('UTS','UTS'),('UAS','UAS'),('Rapat','Rapat'),
			('Wawancara','Wawancara')]

class Schedule(models.Model):
	activity = models.CharField(max_length=100)
	dates = models.DateField()
	time = models.TimeField()
	location = models.CharField(max_length=100)
	category = models.CharField(max_length=100, choices=Category)