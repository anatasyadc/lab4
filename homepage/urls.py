from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
	path('', views.index, name = "index"),
	path('about/', views.about, name = "about"),
	path('skill/', views.skill, name = "skill"),
	path('experience/', views.experience, name = "experience"),
	path('galeri/', views.galeri, name = "galeri"),
	path('contact/', views.contact, name = "contact"),
	path('schedule_add', views.schedule_add, name="schedule_add"),
	path('schedule_detail', views.schedule_detail, name="schedule_detail"),
	path('schedule_edit/<int:pk>', views.schedule_edit, name='schedule_edit'),
	path('schedule_delete/<int:pk>', views.schedule_delete, name='schedule_delete'),
	path('del_schedule', views.del_schedule, name='del_schedule'),
]