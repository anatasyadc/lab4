from django.shortcuts import render
from .forms import ScheduleForm
from .models import Schedule
from django.shortcuts import redirect
import datetime

# Create your views here.
def index(request):
	return render(request, 'index.html')

def about(request):
	return render(request, 'about.html')

def skill(request):
	return render(request, 'skill.html')

def experience(request):
	return render(request, 'experience.html')

def galeri(request):
	return render(request, 'galeri.html')

def contact(request):
	return render(request, 'contact.html')

def schedule_add(request):
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('homepage:schedule_detail')
    else:
        form = ScheduleForm()

    content = {'title' : 'Form Schedule',
                'form' : form}

    return render(request, 'schedule_form.html', content)

def schedule_detail(request):
    data = Schedule.objects.all()
    content = {'title' : 'Schedule',
                'data' : data}
    return render(request, 'schedule.html', content)

def schedule_edit(request, pk):
    post = Schedule.objects.get(pk=pk)
    if request.method == "POST":
        form = ScheduleForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('homepage:schedule_detail')
    else:
        form = ScheduleForm(instance=post)

    content = {'title' : 'Form Schedule',
                'form' : form,
                'obj' : post}
    return render(request, 'schedule_edit.html', content)

def schedule_delete(request, pk):
    Schedule.objects.filter(pk=pk).delete()
    data = Schedule.objects.all()
    content = {'title' : 'Schedule',
                'data' : data}
    return redirect('homepage:schedule_detail')

def del_schedule(request):
    if request.method == "POST":
        Schedule.objects.all().delete()
        data = Schedule.objects.all()
        content = {'title' : 'Schedule',
                'data' : data}
        return redirect('homepage:schedule_detail')
    return redirect('homepage:schedule_detail')
